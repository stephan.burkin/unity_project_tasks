﻿namespace ConstantsValue
{
    public static class AssetsPath
    {
        public const string UIRootPath = "UI/UIRoot";
        public const string Hud = "UI/Hud";
        public const string WindowsDataPath = "StaticData/Windows/WindowsStaticData";
        public const string HeroSpawnDataPath = "StaticData/Hero/SpawnData";
        public const string HeroCharacteristicsDataPath = "StaticData/Hero/HeroBaseStaticData";
        public const string ItemsDataPath = "StaticData/Items/KeyMandatoryInventoryItemStaticData";
        public const string DoorDataPath = "StaticData/Door/DoorStaticData";
        public const string AudioDataPath = "StaticData/Audio/AudioStaticData";
        
        public const string EnemiesDataPath = "StaticData/Enemies";
        public const string LevelsDataPath = "StaticData/Levels";
        public const string LootsDataPath = "StaticData/Loots";
        public const string BonusDataPath = "StaticData/Bonuses";

        public const string ShopDataPath = "StaticData/Shop/ShopStaticData";
        public const string ScoreDataPath = "StaticData/Score/ScoreStaticData";

        public const string MoneyPrefabPath = "Loot/Money";
        public const string DroppedLootPrefabPath = "Loot/DroppedLoot";
    }
}